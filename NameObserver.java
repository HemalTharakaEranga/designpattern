public interface NameObserver {
    void update(String name);
}
