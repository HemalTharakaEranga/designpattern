class Invoker {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void executeCommand(Command command) {
        this.command = command;
        this.command.execute();
    }

    public void undoCommand(Command command) {
        this.command = command;
        this.command.undo();
    }
}
