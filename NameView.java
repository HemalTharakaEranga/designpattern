import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NameView extends JFrame {
    private JLabel label;
    private JButton playButton;
    private JButton continueButton;

    public NameView() {
        setTitle("Abominodo");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(900, 500)); 
        setResizable(false); 
        setLocationRelativeTo(null);


        //label = new JLabel("Welcome!");
        label = new JLabel();
        playButton = new JButton("Play");
        continueButton = new JButton("Continue");

        setLayout(new FlowLayout());
        //add(label);
        add(playButton);
        add(continueButton);
        add(label);

        pack();
        setVisible(true);
    }

    public void addPlayButtonListener(ActionListener listener) {
        playButton.addActionListener(listener);
    }

    public void addContinueButtonListener(ActionListener listener) {
        continueButton.addActionListener(listener);
    }
    
    public void showWelcomeMessage(String message) {
    	label.setText(message);
    }
   // public void showWelcomeMessage(String message) {
        //label.setText(message);
   // }
}
