public class NameConsoleObserver implements NameObserver {
    @Override
    public void update(String name) {
        System.out.println("Welcome, " + name + " Have a good time playing Abominodo!");
    }
}
