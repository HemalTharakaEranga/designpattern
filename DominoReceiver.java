import java.util.ArrayList;
import java.util.List;

public class DominoReceiver{
    private List<Domino1> dominos;

    public DominoReceiver() {
        dominos = new ArrayList<>();
    }

    public void placeDomino(int placeNumber) {
        Domino1 domino = new Domino1(placeNumber);
        dominos.add(domino);
    }

    public void undoPlaceDomino(int placeNumber) {
        dominos.removeIf(domino -> domino.getNumber() == placeNumber);
    }

    public void displayDominos() {
        for (Domino1 domino : dominos) {
            domino.display();
        }
    }
    public boolean isDominoPlaced(int number) {
        for (Domino1 domino : dominos) {
            if (domino.getNumber() == number) {
                return true;
            }
        }
        return false;
    }
}
