import java.util.ArrayList;
import java.util.List;

public class NameModel {
    private List<NameObserver> observers = new ArrayList<>();
    private String name;

    public void setName(String name) {
        this.name = name;
        notifyObservers();
    }

    public String getName() {
        return name;
    }

    public void registerObserver(NameObserver observer) {
        observers.add(observer);
    }

    public void notifyObservers() {
        for (NameObserver observer : observers) {
            observer.update(name);
        }
    }
}
