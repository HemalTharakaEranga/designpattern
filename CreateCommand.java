public class CreateCommand implements Command {
    private DominoReceiver receiver;
    private int placeNumber;

    public CreateCommand(DominoReceiver receiver, int placeNumber) {
        this.receiver = receiver;
        this.placeNumber = placeNumber;
    }

    public void execute() {
        if (placeNumber >= 0 && placeNumber <= 7) {
            if (receiver.isDominoPlaced(placeNumber)) {
                System.out.println("Domino already placed for number " + placeNumber + ". Please enter a different number.");
            } else {
                receiver.placeDomino(placeNumber);
                System.out.println("Domino placed for number " + placeNumber);
            }
        } else {
            System.out.println("Invalid domino number. Please enter a number between 0 and 7.");
        }
    }

    public void undo() {
        receiver.undoPlaceDomino(placeNumber);
    }
}
