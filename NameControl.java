import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class NameControl {
	private NameModel model;
    private NameView view;
    private Scanner scanner;

    public NameControl(NameModel model, NameView view) {
        this.model = model;
        this.view = view;
        this.scanner = new Scanner(System.in);

        view.addPlayButtonListener(new PlayButtonListener());
        view.addContinueButtonListener(new ContinueButtonListener());
    }

    class PlayButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
        	view.showWelcomeMessage("Welcome To Abominodo - The Best Dominoes Puzzle Game in the Universe\n"
        		    + "\nVersion 1.0 (c), Kevan Buckley, 2010");
        }
    }

    class ContinueButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.print("Enter your name: ");
            String name = scanner.nextLine();
            model.setName(name);
        }
    }
    
}
