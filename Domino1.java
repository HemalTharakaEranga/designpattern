public class Domino1 {
    private int number;

    public Domino1(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void display() {
        System.out.println("Domino: " + number);
    }
 
}
