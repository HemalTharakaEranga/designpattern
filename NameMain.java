public class NameMain {
    public static void main(String[] args) {
        NameModel model = new NameModel();
        NameView view = new NameView();
        NameControl controller = new NameControl(model, view);

        model.registerObserver(new NameConsoleObserver());
    }
}
