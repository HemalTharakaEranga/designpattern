import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
	    DominoReceiver receiver = new DominoReceiver();
	    Invoker invoker = new Invoker();

	    Scanner scanner = new Scanner(System.in);

	    while (true) {
	        System.out.print("Enter the number of the domino to place (0-7): ");
	        int placeNumber = scanner.nextInt();
	        scanner.nextLine(); // Consume the newline character

	        Command command = new CreateCommand(receiver, placeNumber);
	        invoker.executeCommand(command);

	        //System.out.print("Do you want to place another domino? (y/n): ");
	        //String choice = scanner.nextLine();
	        //if (!choice.equalsIgnoreCase("y")) {
	            //break;
	        //}
	    }

	    //System.out.print("Do you want to undo any placed dominos? (y/n): ");
	    //String undoChoice = scanner.nextLine();
	    //if (undoChoice.equalsIgnoreCase("y")) {
	        //System.out.print("Enter the number of the domino to undo: ");
	        //int undoNumber = scanner.nextInt();
	        //scanner.nextLine(); // Consume the newline character

	        //Command undoCommand = new UndoCommand(receiver, undoNumber);
	        //invoker.undoCommand(undoCommand);
	    //}

	    //receiver.displayDominos();

	    //scanner.close();
	}
}

