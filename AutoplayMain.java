import java.util.Scanner;

public class AutoplayMain {
    public static void main(String[] args) {
        Context context = new Context();
        Scanner scanner = new Scanner(System.in);
        
        Strategy operation1 = new Operation1();
        context.setStrategy(operation1);
        context.executeStrategy();

        Strategy operation2 = new Operation2();
        context.setStrategy(operation2);
        context.executeStrategy();

        Strategy operation3 = new Operation3();
        context.setStrategy(operation3);
        context.executeStrategy();
        
        System.out.println("");
        int input = scanner.nextInt();
        
        if (input == 1) {
            System.out.println("Let's Play");
        } else if (input == 2) {
            System.out.println("Thank you!");
            System.out.println("%s your score is");
        } else if (input == 3) {
            System.out.println("Thank you for playing");
        } else if (input > 3) {
            System.out.println("It is a shame that you did not want to play");
        } else {
            System.out.println("Wrong number!");
        }


    }
}
